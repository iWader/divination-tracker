## Surreal Divination Tracker

I built this for [Clan Surreal](http://www.clansurreal.com) using the [Laravel 4 PHP Framework](https://github.com/laravel/laravel) to run a month long competition when the new skill on MMORPG [RuneScape](http://www.runescape.com) is released later this month.

As of writing (11 Aug 2013) this is incredibly untested due to the fact said skill doesn't exist.

## License

This software is licensed under the [MIT license](http://opensource.org/licenses/MIT)