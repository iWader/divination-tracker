<html>
    <head>
        <title>Surreal Divination Tracker</title>
        <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
        <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        
        <style type="text/css">
            body {
                margin: auto;
                width: 960px;
            }
        </style>
        
        <script type="text/javascript">
            google.load("visualization", "1", {packages:["corechart"]});
            google.setOnLoadCallback(drawXpChart);
            google.setOnLoadCallback(drawRankChart);
            
            function drawXpChart()
            {
                var dataTable = new google.visualization.DataTable();
                dataTable.addColumn('datetime', 'Date');
                dataTable.addColumn('number', 'XP');
                dataTable.addRows([
                @foreach($tracker_data as $data)
                    [new Date("{{ $data['timestamp'] }}"), {{ $data['exp'] }}],
                @endforeach
                ]);
                
                var dataView = new google.visualization.DataView(dataTable);
                var chart = new google.visualization.LineChart(document.getElementById('xp_gain_chart'));
                var options = {
                    width: 1000, height: 600,
                    legend: 'none',
                    pointSize: 1,
                    title: 'XP Gain',
                    hAxis: {format:'MMM d'}
                };
                chart.draw(dataView, options);
            }
            
            function drawRankChart()
            {
                var dataTable = new google.visualization.DataTable();
                dataTable.addColumn('datetime', 'Date');
                dataTable.addColumn('number', 'Rank');
                dataTable.addRows([
                @foreach($tracker_data as $data)
                    [new Date("{{ $data['timestamp'] }}"), {{ $data['rank'] }}],
                @endforeach
                ]);
                
                var dataView = new google.visualization.DataView(dataTable);
                var chart = new google.visualization.LineChart(document.getElementById('rank_gain_chart'));
                var options = {
                    width: 1000, height: 600,
                    legend: 'none',
                    pointSize: 1,
                    title: 'Rank Gain',
                    hAxis: {format:'MMM d'}
                };
                chart.draw(dataView, options);
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="page-header clearfix">
                <img src="http://services.runescape.com/m=avatar-rs/{{ urlencode($profile['rsn']) }}/chat.png" class="pull-left">
                
                <a href="{{ URL::to('') }}"><h1>Surreal Divination Tracker</h1></a>
                <a href="{{ URL::to('u/' . urlencode($profile['rsn'])) }}"><h4>{{ $profile['rsn'] }}</h4></a>
            </div>
            <div class="row-fluid">
                <div class="span4">
                    <table class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <th>Timestamp</th>
                                <th>Exp</th>
                                <th>Level</th>
                                <th>Rank</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach(array_reverse($tracker_data) as $data)
                            <tr>
                                <td>{{ $data['timestamp'] }}</td>
                                <td>{{ number_format($data['exp']) }}</td>
                                <td>{{ $data['level'] }}</td>
                                <td>{{ number_format($data['rank']) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="span8">
                    <div id="xp_gain_chart"></div>
                    
                    <div id="rank_gain_chart"></div>
                </div>
            </div>
        </div>
    </body>
</html>