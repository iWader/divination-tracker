<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Laravel PHP Framework</title>
    <style>
        @import url(//fonts.googleapis.com/css?family=Lato:300,400,700);

        body {
            margin:0;
            font-family:'Lato', sans-serif;
            text-align:center;
            color: #999;
        }

        .splash {
           width: 560px;
           height: 350px;
           position: absolute;
           left: 50%;
           top: 50%; 
           margin-left: -280px;
           margin-top: -175px;
        }

        a, a:visited {
            color:#FF5949;
            text-decoration:none;
        }

        a:hover {
            text-decoration:underline;
        }
    </style>
</head>
<body>
    <div class="splash">
        <h1>Divination</h1>
        <iframe width="560" height="315" src="//www.youtube.com/embed/hWYYuLNMAA4" frameborder="0" allowfullscreen></iframe>
    </div>
</body>
</html>