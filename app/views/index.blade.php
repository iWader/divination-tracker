<html>
    <head>
        <title>Surreal Divination Tracker</title>
        <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
        <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
        
        <style type="text/css">
            body {
                margin: auto;
                width: 960px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="page-header">
                <h1>Surreal Divination Tracker</h1>
                <p>Surreal is running a month long competition for the release of the new skill Divination. Who will be The God among Gods?</p>
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>RSN</th>
                        <th>XP</th>
                        <th>Level</th>
                    </tr>
                </thead>
                <tbody>
                @for ($i = 0; $i < count($leaderboard); $i++)
                    <tr>
                        <td>{{ ordinalNum($i + 1) }}</td>
                        <td><a href="{{ URL::to('u/' . urlencode($leaderboard[$i]['rsn'])) }}">{{ $leaderboard[$i]['rsn'] }}</a></td>
                        <td>{{ number_format($leaderboard[$i]['exp']) }}</td>
                        <td>{{ $leaderboard[$i]['level'] }}</td>
                    </tr>
                @endfor
                </tbody>
            </table>
        </div>
    </body>
</html>