<?php

class HomeController extends BaseController
{

    public function index()
    {
        if (DB::table('users')->count() == 0)
        {
            return View::make('splash');
        }
        
        $Tracker = new Tracker();
        
        return View::make('index', array('leaderboard' => $Tracker->getLeaderboard()));
    }
    
    public function showUser(RSN $RSN)
    {
        return View::make('profile', array('profile' => $RSN->toArray(), 'tracker_data' => $RSN->tracker()->get()->toArray()));
    }

}