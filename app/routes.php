<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Define route models
Route::model('rsn', 'RSN');
Route::bind('rsn', function($value, $route)
{
    return RSN::where('rsn', $value)->first();
});

Route::get('/', 'HomeController@index');
Route::get('u/{rsn}', 'HomeController@showUser')->where('rsn', '[a-zA-Z1-9\-_ ]{1,12}');