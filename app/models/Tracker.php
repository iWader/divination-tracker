<?php

class Tracker extends Eloquent
{
    
    /**
     * The database table used by the model.
     * 
     * @var string
     */
    protected $table = 'tracker';
    
    /**
     * Attributes excluded from the models JSON form
     * 
     * @var array
     */
    protected $hidden = array();
    
    /**
     * Attributes automatically filled from the database
     * 
     * @var array
     */
    protected $fillable = array('timestamp', 'rank', 'level', 'exp');
    
    /**
     * Attributes protected from mass assignment.
     * 
     * @var array
     */
    protected $guarded = array('id', 'user_id');
    
    /**
     * Trackable skills
     * 
     * @var array
     */
    public static $skills = array("overall", "attack", "defence", "strength", "constitution", "ranged", "prayer", "magic", "cooking", "woodcutting", "fletching", "fishing", "firemaking", "crafting", "smithing", "mining", "herblore", "agility", "thieving", "slayer", "farming", "runecrafting", "hunter", "construction", "summoning", "dungeoneering", "divination");
    
    /**
     * User relationship
     * 
     * @return RSN
     */
    public function RSN()
    {
        return $this->belongsTo('RSN');
    }
    
    /**
     * Retrieve the leaderboard
     * 
     * @var array
     */
    public function getLeaderboard()
    {
        return DB::select("SELECT `rsn`, `rank`, `level`, `exp` FROM `tracker` JOIN `users` ON `tracker`.`user_id` = `users`.`id` GROUP BY `rsn` ORDER BY `exp` DESC");
    }
    
}
