<?php

class RSN extends Eloquent
{
    
    /**
     * The database table used by the model.
     * 
     * @var string
     */
    protected $table = 'users';
    
    /**
     * Attributes excluded from the models JSON form
     * 
     * @var array
     */
    protected $hidden = array();
    
    /**
     * Attributes automatically filled from the database
     * 
     * @var array
     */
    protected $fillable = array('rsn');
    
    /**
     * Attributes protected from mass assignment.
     * 
     * @var array
     */
    protected $guarded = array('id');
    
    /**
     * Tracker relationship
     * 
     * @return Tracker
     */
    public function tracker()
    {
        return $this->hasMany('Tracker', 'user_id');
    }
    
}