<?php

use Illuminate\Database\Migrations\Migration;

class TrackerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tracker', function($t)
        {
            $t->engine = 'MyISAM';
            
            $t->increments('id');
            $t->integer('user_id')->referances('id')->on('users');
            $t->timestamp('timestamp');
            $t->integer('rank')->unsigned();
            $t->integer('level');
            $t->integer('exp')->unsigned();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tracker');
	}

}