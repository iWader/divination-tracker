<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class updateStats extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'srl:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update users stats.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$users = DB::select("SELECT `rsn` FROM `users` WHERE `deleted_at` IS NULL");
        
        foreach ($users as $user)
        {
            $stats = $this->getStats($user->rsn);
            
            if ($stats == false)
                continue;
            
            DB::insert(
                "INSERT INTO `tracker` (`user_id`, `timestamp`, `rank`, `level`, `exp`) VALUES ( (SELECT `id` FROM `users` WHERE `rsn` = ?), NOW(), ?, ?, ?)",
                array($user->rsn, $stats['divination']['rank'], $stats['divination']['level'], $stats['divination']['exp'])
            );
        }
	}
    
    /**
     * Fetch the users stats
     * 
     * @return array
     */
    private function getStats($user)
    {
        $fails = 0;
        
        for ($i = 0; $i <= 3; $i++)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://hiscore.runescape.com/index_lite.ws?player=" . urlencode($user));
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
            $file = curl_exec($ch);
            
            if (!curl_getinfo($ch, CURLINFO_HTTP_CODE) == "200")
            {
                $fails++;
                continue;
            }
            
            // Jagex don't know how to send 404 headers
            if (preg_match("~<(/)?html>~", $file))
            {
                $fails++;
                continue;
            }
            
            $file = explode("\n", $file);
            
            for ($j = 0; $j < count(Tracker::$skills); $j++)
            {
                $stat = explode(",", $file[$j]);
                
                $stats[Tracker::$skills[$j]]['rank'] = $stat[0];
                $stats[Tracker::$skills[$j]]['level'] = $stat[1];
                $stats[Tracker::$skills[$j]]['exp'] = $stat[2];
            }
        }
        
        if (empty($stats))
            return false;
            
        return $stats;
    }

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		);
	}

}