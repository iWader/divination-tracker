<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class getUsers extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'srl:getUsers';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Get users from the Surreal Hub API.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command. Fetch new data from the Surreal Hub API
	 *
	 * @return void
	 */
	public function fire()
	{
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://clansurreal.com/board/hub/api.php");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $file = curl_exec($ch);
        
        $file = json_decode($file);
        
        foreach ($file as $user)
        {
            DB::insert("INSERT IGNORE INTO `users` (`rsn`) VALUES (?)", array($user->runescape_name));
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		);
	}

}